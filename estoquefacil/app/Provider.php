<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = ["cnpj", "razaosocial", "cep", "rua", "numero", "complemento", "bairro", "cidade", "uf", "telefone", "celular", "email", "observacoes"];

}
