<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("cnpj", 14)->unique();
            $table->string("razaosocial");
            $table->bigInteger("cep");
            $table->string("rua");
            $table->integer("numero");
            $table->string("complemento");
            $table->string("bairro");
            $table->string("cidade");
            $table->string("uf");
            $table->string("telefone");
            $table->string("celular");
            $table->string("email");
            $table->longText("observacoes");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
